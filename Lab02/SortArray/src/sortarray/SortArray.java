package sortarray;

import java.util.Scanner;

public class SortArray {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter n: ");
        int n = sc.nextInt();
        int[] arr = new int[n];
        
        for(int i = 0; i < n; i++){
            System.out.print("arr[" + i + "} = ");
            arr[i] = sc.nextInt();
        }
        
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i] > arr[j]){
                    int t = arr[i];
                    arr[i] = arr[j];
                    arr[j] = t;
                }
            }
        }
        
        int sum = 0;
        for(int i = 0; i < n; i++){
            System.out.print(arr[i] + " ");
            sum += arr[i];
        }
        System.out.println("\nSum array: " + sum);
        System.out.println("Average value: " + sum/n);        
    }
}
