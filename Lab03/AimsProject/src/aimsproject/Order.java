package aimsproject;

public class Order {
    
    public static final int MAX_NUMBERS_ORDERED = 10;
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    private int qtyOrdered = 0;

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }
    
    public void addDigitalVideoDisc(DigitalVideoDisc disc){
        if(qtyOrdered < MAX_NUMBERS_ORDERED){
            itemsOrdered[qtyOrdered] = disc;
            qtyOrdered++;
            System.out.println("The disc has been added");
        }else{
            System.out.println("The order is almost full");
        }
    }
    
    public void removeDigitalVideoDisc(DigitalVideoDisc disc){
        for(int i = 0; i <=MAX_NUMBERS_ORDERED; i++){
            if(itemsOrdered[i] == disc){
                qtyOrdered--;
                for(int j = i; j < MAX_NUMBERS_ORDERED - 1; j++){
                    itemsOrdered[j] = itemsOrdered[j+1];
                }
                System.out.println("The disc has been remove");
                break;
            }
        }
    }
    
    public float totalCOst(){
        float sum = 0;
        for(int i = 0; i < qtyOrdered; i++){
            sum += itemsOrdered[i].getCost();
        }
        return sum;
    }
}
