package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.factory.BookCreation;
import hust.soict.hedspi.aims.media.factory.CDCreation;
import hust.soict.hedspi.aims.media.factory.DVDCreation;
import hust.soict.hedspi.aims.media.factory.MediaCreation;
import hust.soict.hedspi.aims.order.Order;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Aims {

    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Back");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void showAdminMenu() {
        System.out.println("Product Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new item");
        System.out.println("2. Delete item by id");
        System.out.println("3. Display the items list");
        System.out.println("0. Back");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3");
    }

    public static void showUserMenu() {
        System.out.println("Welcome to AIMS Store: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Search for an item from the list by title");
        System.out.println("3. Add item to order by id (id in the list of available items of the store)");
        System.out.println("4. Remove item from order by id (id in the order)");
        System.out.println("5. Display the order information");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4-5");
    }

    public static Media createMedia(MediaCreation mc) {
        return mc.createMediaFromConsole();
    }
    
    public static void showCreationMenu(List<Media> mediaList){
        System.out.println("-------------------------");
        System.out.println("1. Create DVD");
        System.out.println("2. Create CD");
        System.out.println("3. Create Book");
        System.out.println("0. Exit");
        System.out.println("-------------------------");
        Scanner sc = new Scanner(System.in);
        int type = Integer.parseInt(sc.nextLine());
        switch(type){
            case 1:
                DigitalVideoDisc dvd = (DigitalVideoDisc) createMedia(new DVDCreation());
                mediaList.add(dvd);
                break;
            case 2:
                CompactDisc cd = (CompactDisc) createMedia(new CDCreation());
                mediaList.add(cd);
                break;
            case 3:
                Book book = (Book) createMedia(new BookCreation());
                mediaList.add(book);
                break;
            case 0:
            default:
                break;
        }
    }

    public static void main(String[] args) {
        Order o1 = null;
        List<Media> mediaList = new ArrayList<Media>();
        
        int system;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("System");
            System.out.println("1. Admin");
            System.out.println("2. User");
            System.out.println("0. Exit");
            system = sc.nextInt();
            switch (system) {
                case 0:
                    System.out.println("Exit!");
                    System.exit(0);
                case 1:
                    int admin;
                    do {
                        showAdminMenu();
                        admin = sc.nextInt();
                        switch (admin) {
                            case 1:
                                showCreationMenu(mediaList);
                                break;
                            case 2:
                                System.out.println("Enter the id of item you want to delete: ");
                                int id = sc.nextInt();
                                for(int i=0; i<mediaList.size(); i++){
                                    if(mediaList.get(i).getId() == id){
                                        mediaList.remove(i);
                                    }
                                }
                                break;
                            case 3:
                                for(int i=0; i<mediaList.size(); i++){
                                    System.out.println(mediaList.get(i).toString());
                                }
                                break;
                            case 0:
                                break;
                            default:
                                break;
                        }
                    } while (admin != 0);
                    break;
                case 2:
                    int chose;
                    do {
                        //showMenu();
                        showUserMenu();
                        chose = sc.nextInt();

                        switch (chose) {
                            case 1:
                                o1 = Order.createOrder();
                                System.out.println("Order has been created");
                                break;
                            case 2:
                                System.out.println("Enter title you want to search: ");
                                Scanner s = new Scanner(System.in);
                                String title = s.nextLine();
                                for(int i =0; i< mediaList.size(); i++){
                                    if(title.equals(mediaList.get(i).getTitle())){
                                        System.out.println(mediaList.get(i).toString());
                                    }
                                }
                                break;
                            case 3:
                                System.out.println("Enter id of the item you want to add: ");
                                s = new Scanner(System.in);
                                int id = Integer.parseInt(s.nextLine());
                                for(int i=0; i<mediaList.size(); i++){
                                    if(mediaList.get(i).getId() == id){
                                        o1.addMedia(mediaList.get(i));
                                    }
                                }
                                break;
                            case 4:
                                System.out.println("Id of item you want to remove: ");
                                s = new Scanner(System.in);
                                id = Integer.parseInt(s.nextLine());
                                o1.removeMedia(o1.getOrderItems().get(id));
                                break;
                            case 5:
                                o1.printOrder();
                                break;
                            case 0:
                                break;
                            default:
                                break;
                        }

                    } while (chose != 0);
            }
        } while (system != 0);

    }
}
