package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.order.Order;
import java.util.Scanner;

public class Aims {

    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void main(String[] args) {
        Order o1 = null;

        Media item = new Media();
        int chose;
        Scanner sc = new Scanner(System.in);

        do {
            showMenu();
            chose = sc.nextInt();

            switch (chose) {
                case 1:
                    o1 = Order.createOrder();
                    System.out.println("Order has been created");
                    break;
                case 2:
                    int media_type;
                    System.out.println("0. DVD");
                    System.out.println("1. CD");
                    System.out.println("2. Book");
                    System.out.println("Chose type of item:");
                    media_type = sc.nextInt();
                    switch (media_type) {
                        case 0: case 1: case 2:
                            o1.addMedia(media_type);
                            break;
                        default:
                            break;
                    }
                    break;
                case 3:
                    System.out.println("Id of item you want to delete: ");
                    int id = sc.nextInt();
                    o1.removeMedia(o1.getOrderItems().get(id));
                    break;
                case 4:
                    o1.printOrder();
                    break;
                case 0:
                    System.out.println("Exit!");
                    System.exit(0);
                default:
                    break;
            }
        
    }
    while (chose 
!= 0);

//        String str1 = new String("Hello");
//        String str2 = str1;
//        str1 = str1+ " World";
//        
//        System.out.println("String testing: ");
//        System.out.println(str1);
//        System.out.println(str2);
//        
//        System.out.println("DVD object testing");
//        DigitalVideoDisc d1= new DigitalVideoDisc("Jungle");
//        DigitalVideoDisc d2 = d1;
//        d1.setTitle(d1.getTitle()+" Legends");
//        System.out.println(d1.getTitle());
//        System.out.println(d2.getTitle());
    }
}
