package hust.soict.hedspi.garbage;

import java.io.*;
import java.util.Random;
import jdk.jshell.execution.Util;

public class ConcatenationInLoops {

    public static void main(String[] args) throws FileNotFoundException, IOException {
//        File file = new File("C:\\Users\\DMX\\Documents\\20212\\TH OOP\\Week 5\\OtherProjects\\src\\hust\\soict\\hedspi\\garbage\\t.txt");
//        InputStream inputStream = new FileInputStream(file);
//        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//        BufferedReader reader = new BufferedReader(inputStreamReader);

        FileInputStream fin = null;
        try {
            fin = new FileInputStream("C:\\Users\\DMX\\Documents\\20212\\TH OOP\\Week 5\\OtherProjects\\src\\hust\\soict\\hedspi\\garbage\\t.txt");
            int ch;
            String s = "";
            StringBuilder sb = new StringBuilder();
            StringBuffer sb2 = new StringBuffer();
            long start = System.currentTimeMillis();
            while ((ch = fin.read()) != -1) {
                //s += (char)ch;
                //sb.append((char)ch);
                sb2.append((char)ch);
            }
            System.out.println(System.currentTimeMillis() - start);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fin != null) {
                    fin.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

//
//        r = new Random(123);
//        start = System.currentTimeMillis();
//        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < 65536; i++) {
//            sb.append(r.nextInt(2));
//        }
//        s = sb.toString();
//        System.out.println(System.currentTimeMillis() - start);
//        
//        r = new Random(123);
//        start = System.currentTimeMillis();
//        StringBuffer sb2 = new StringBuffer();
//        for (int i = 0; i < 65536; i++) {
//            sb2.append(r.nextInt(2));
//        }
//        s = sb2.toString();
//        System.out.println(System.currentTimeMillis() - start);
    }
}
