package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Book extends Media{
    
    private List<String> authors = new ArrayList<String>();

    public Book() {
    }

    public Book(String title) {
        super(title);
    }

    public Book(String title, String category) {
        super(title, category);
    }
    
    public Book(String title, String category, float cost) {
        super(title, category, cost);
    }
    
    public Book(String title, String category, List<String> authors) {
        super(title, category);
        if(authors.isEmpty()){
            System.out.println("Authors is null");
            return;
        }
        this.authors = authors;
    }
    
    public Book(String title, String category, float cost, List<String> authors) {
        super(title, category, cost);
        this.authors = authors;
    }

    public void addAuthor(String author){
        this.authors.add(author);
    }
    
    public void addAuthors(List<String> authors){
        this.authors.addAll(authors);
    }
    
    public List<String> getAuthors(){
        return this.authors;
    }
    
    public Media inputInformation(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Media Title: ");
        String title = sc.nextLine();
        System.out.println("Media Category: ");
        String category = sc.nextLine();
        System.out.println("Media Cost");
        float cost = Float.parseFloat(sc.nextLine());
        System.out.println("Book Author: ");
        String author = sc.nextLine();
        authors = Arrays.asList(author.split(","));
        return new Book(title, category, cost, authors);
    }
    
    @Override
    public String toString() {
        return "Book - " + super.toString() + " - Authors " + this.getAuthors().toString();
    }
}
