package hust.soict.hedspi.aims.media.factory;

import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.disc.Track;
import hust.soict.hedspi.aims.media.Media;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CDCreation implements MediaCreation{

    @Override
    public Media createMediaFromConsole() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Media ID: ");
        int id = Integer.parseInt(sc.nextLine());
        System.out.println("Media Title: ");
        String title = sc.nextLine();
        System.out.println("Media Category: ");
        String category = sc.nextLine();
        System.out.println("CD Director: ");
        String director = sc.nextLine();
        System.out.println("CD Length: ");
        int length = Integer.parseInt(sc.nextLine());
        System.out.println("CD Artist: ");
        String artist = sc.nextLine();
        System.out.println("Media Cost");
        float cost = Float.parseFloat(sc.nextLine());
        System.out.println("The number of track: ");
        int n = sc.nextInt();
        List<Track> list = new ArrayList<Track>(n);
        for(int i=0; i<n; i++){
            Track t = new Track().inputInformation();
            list.add(t);
        }
        return new CompactDisc(id, title, category, cost, director, length, artist, list);
    }
    
}
