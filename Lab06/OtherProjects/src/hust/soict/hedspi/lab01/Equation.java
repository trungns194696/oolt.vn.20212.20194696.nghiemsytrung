import javax.swing.JOptionPane;

public class Equation {
    public static void main(String[] args){
        String strNum1, strNum2, strNum3;
        String strNum4, strNum5, strNum6;

        String key = JOptionPane.showInputDialog(null, "1. The first-degree equation with one vatiable" +
                    "\n2. The system of first-degree quation with two variables" + 
                    "\n3. The second-degree equation with one variable", "Equation", JOptionPane.INFORMATION_MESSAGE);
        
        int k = Integer.parseInt(key);
        switch(k){
            case 1:
                strNum1 = JOptionPane.showInputDialog(null, "Please input a: ", "ax + b = 0", JOptionPane.INFORMATION_MESSAGE);
                strNum2 = JOptionPane.showInputDialog(null, "Please input b: ", "ax + b = 0", JOptionPane.INFORMATION_MESSAGE);
        
                double num1 = Double.parseDouble(strNum1);
                double num2 = Double.parseDouble(strNum2);
        
                if(num1 == 0){
                    JOptionPane.showMessageDialog(null, "The equation has no solution!", "ax + b = 0", JOptionPane.INFORMATION_MESSAGE);
                }else{
                    JOptionPane.showMessageDialog(null, "The equation has one solution x = " + ((-num2) / num1), "ax + b = 0", JOptionPane.INFORMATION_MESSAGE);
                }
                break;
            case 2:
                strNum1 = JOptionPane.showInputDialog(null, "Please input a11: ", "a11x1 + a12x2 = b1", JOptionPane.INFORMATION_MESSAGE);
                strNum2 = JOptionPane.showInputDialog(null, "Please input a12: ", "a11x1 + a12x2 = b1", JOptionPane.INFORMATION_MESSAGE);
                strNum3 = JOptionPane.showInputDialog(null, "Please input b1: ", "a11x1 + a12x2 = b1", JOptionPane.INFORMATION_MESSAGE);
                strNum4 = JOptionPane.showInputDialog(null, "Please input a21: ", "a21x1 + a22x2 = b2", JOptionPane.INFORMATION_MESSAGE);
                strNum5 = JOptionPane.showInputDialog(null, "Please input a22: ", "a21x1 + a22x2 = b2", JOptionPane.INFORMATION_MESSAGE);
                strNum6 = JOptionPane.showInputDialog(null, "Please input b2: ", "a21x1 + a22x2 = b2", JOptionPane.INFORMATION_MESSAGE);
        
                num1 = Double.parseDouble(strNum1);
                num2 = Double.parseDouble(strNum2);
                double num3 = Double.parseDouble(strNum1);
                double num4 = Double.parseDouble(strNum2);
                double num5 = Double.parseDouble(strNum1);
                double num6 = Double.parseDouble(strNum2);
        
                double d, d1, d2;
                d = num1 * num5 - num4 * num2;
                d1 = num3 * num5 - num6 * num2;
                d2 = num1 * num6 - num4 * num3;
        
                if(d == 0){
                    JOptionPane.showMessageDialog(null, "The equation has no solution!", "a11x1 + a12x2 = b1 and a21x1 + a22x2 = b2", JOptionPane.INFORMATION_MESSAGE);
                }else{
                    JOptionPane.showMessageDialog(null, "The equation has solution (x1, x2) = (" + d1 / d +"; " + d2 / d +")", "ax + b = 0", JOptionPane.INFORMATION_MESSAGE);
                }
                break;
            case 3:
                strNum1 = JOptionPane.showInputDialog(null, "Please input a: ", "ax^2 + bx = c", JOptionPane.INFORMATION_MESSAGE);
                strNum2 = JOptionPane.showInputDialog(null, "Please input b: ", "ax^2 + bx = c", JOptionPane.INFORMATION_MESSAGE);
                strNum3 = JOptionPane.showInputDialog(null, "Please input c: ", "ax^2 + bx = c", JOptionPane.INFORMATION_MESSAGE);
                
                num1 = Double.parseDouble(strNum1);
                num2 = Double.parseDouble(strNum2);
                num3 = Double.parseDouble(strNum1);
                
                double delta = num2 * num2 - 4*num1*num3;
        
                if (num1 == 0){
                    JOptionPane.showMessageDialog(null, "The equation has one solution x = " + ((-num3) / num2), "ax^2 + bx = c", JOptionPane.INFORMATION_MESSAGE);
                }else if(delta < 0){
                    JOptionPane.showMessageDialog(null, "The equation has no solution!", "ax^2 + bx = c", JOptionPane.INFORMATION_MESSAGE);
                }else if(delta == 0){
                    JOptionPane.showMessageDialog(null, "The equation has solution x1 = x2 = " + ((-num2) / 2*num1), "ax^2 + bx = c", JOptionPane.INFORMATION_MESSAGE);
                }else{
                    JOptionPane.showMessageDialog(null, "The equation has solution x1 = " + ((-num2 + Math.sqrt(delta)) / 2*num1) + " , x2 = " + ((-num2 - Math.sqrt(delta)) / 2*num1), "ax^2 + bx = c", JOptionPane.INFORMATION_MESSAGE);
                }
                break;
            default:
                break;
        }
        System.exit(0);
    }
}
